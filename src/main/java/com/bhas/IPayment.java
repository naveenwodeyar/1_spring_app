package com.bhas;

public interface IPayment 
{
	public abstract boolean payBill(Double d);
}
