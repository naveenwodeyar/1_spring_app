package com.bhas;

public class PaymentProcessor 
{
	private IPayment iPayment;
	
	// constructor DI,
	public PaymentProcessor(IPayment payment)
	{
		System.out.println("Constructor::DI");
		this.iPayment = payment;
	}
	
	// setter DI,
	public void setPayment(IPayment payment)
	{
		System.out.println("Setter::DI");
		this.iPayment = payment;
	}
	
	public void doPayment(Double amt)
	{
		boolean isDone = iPayment.payBill(amt);
		
			if(isDone)
				System.out.println("\n Payment completed successfully,,");
			else
				System.out.println("\n Sorry unable to process the payment,");
	}
}
