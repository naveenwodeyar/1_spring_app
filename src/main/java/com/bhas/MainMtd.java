package com.bhas;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jndi.support.SimpleJndiBeanFactory;

public class MainMtd 
{

	public static void main(String[] args) 
	{
		// classPath, Loading bean configuration file,
		Resource resource = new ClassPathResource("beans.xml");
		
	    // Starting IOC,reads the bean definitions and creates an map(bean name->className)
//		BeanFactory beanFactory = new XmlBeanFactory(resource);	
					
					// requesting the objects from IOC,
//					beanFactory.getBean("credit",CreditCardPayment.class);		
		
		ApplicationContext app = new ClassPathXmlApplicationContext("Bean.xml");
		PaymentProcessor bean = (PaymentProcessor) app.getBean("paymentProcessor");
						bean.doPayment(45.0);
	}

}
